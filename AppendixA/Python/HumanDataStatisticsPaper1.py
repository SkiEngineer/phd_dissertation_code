# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 21:48:15 2020

@author: Kiffer Creveling
"""

import pandas as pd
import os
import numpy as np
import seaborn as sns
from statannot import add_stat_annotation
import textwrap
import matplotlib.pyplot as plt
from matplotlib.patches import PathPatch
plt.rcParams['figure.figsize'] = [16, 10]
from scipy import stats
import pdb

# In[Read values from Database]
""" Read from the database """

df = pd.read_csv('Human Data Paper 1 Force Data.csv') # Data from JMP

""" Simplification of code """
HSF = 'HumanStatisticsFigures' # figure directory
mpf_mN = 'Max peel force (mN)' # pandas data frame
ss_mN = 'Steady-state peel force (mN)' # pandas data frame
MmN = 'Maximum peel force (mN)' # figure labels
R = 'Region'
Eq = 'Equator'
Po = 'Posterior'
AG = 'AgeGroup'
A60 = 'Age60'
Aleq60 = r'Age $\leq$ 60'
Ag60 = 'Age $>$ 60'
A = 'Age'
fc = 'failure code' # Original data
FC = 'Failure Type' # converted data for plotting
FCw = 'Type of Failure' # Failure code converted to words for plotting

# Units
A_yrs = 'Age (yr.)'
A_G = 'Age Group (yr.)'

df[mpf_mN] = df['Max [N]']*1000 # Convert to mN
df[ss_mN] = df['SS [N]']*1000 # Convert to mN

# Exclude the cells that have duplicates or have been exculded due to 
# video analysis
df = df[df['Excluded'] != 'yes']

# In[Create AgeGroup bins]
bins = [30, 40, 50, 60, 70, 80]
LAG = ['30-39', '40-49', '50-59', '60-69','70-79'] # labels for each age group
# Create binned AgeGroups
df[AG] = pd.cut(df[A], bins, labels=LAG, right=False)

bins = [0, 60, 90]
labelsAge60 = [Aleq60, Ag60]
# Create binned AgeGroups
df[A60] = pd.cut(df[A], bins, labels=labelsAge60, right=True)

# Create Text labels for ILM type of failure
ILM_Failure_Type = [r'ILM Clean Separation', 
                    r'ILM Traction with Undulation', 
                    r'Localized ILM Disruption', 
                    r'Widespread ILM Disruption']

def handle_text(idx):
    if idx == 0:
        return ILM_Failure_Type[0]
    elif idx == 1:
        return ILM_Failure_Type[1]
    elif idx == 2:
        return ILM_Failure_Type[2]
    elif idx == 3:
        return ILM_Failure_Type[3]
    return None

# Create binned ILM disrutptions
df[FCw] = df[fc].apply(handle_text)

# In[Pivot Table]
# create initial pivot table simplifications

pvtOut = {'count', np.median, np.mean, np.std} # pivot table outputs


# In[Plots]

standardError = 68 # Used for confidence intervals

sns.set_theme(context='paper', style='darkgrid', palette="Paired", 
              font_scale=2)
custom_style = {'axes.facecolor': 'white',
                 'axes.edgecolor': 'black',
                 'axes.grid': False,
                 'axes.axisbelow': True,
                 'axes.labelcolor': 'black',
                 'figure.facecolor': 'white',
                 'grid.color': '.8',
                 'grid.linestyle': '-',
                 'text.color': 'black',
                 'xtick.color': 'black',
                 'ytick.color': 'black',
                 'xtick.direction': 'out',
                 'ytick.direction': 'out',
                 'lines.solid_capstyle': 'round',
                 'patch.edgecolor': 'w',
                 'patch.force_edgecolor': True,
                 'image.cmap': 'rocket',
                 'font.family': ['sans-serif'],
                 'font.sans-serif': ['Arial', 'DejaVu Sans', 
                                     'Liberation Sans', 'Bitstream Vera Sans', 
                                     'sans-serif'],
                 'xtick.bottom': True,
                 'xtick.top': False,
                 'ytick.left': True,
                 'ytick.right': False,
                 'axes.spines.left': True,
                 'axes.spines.bottom': True,
                 'axes.spines.right': False,
                 'axes.spines.top': False}
# White background with ticks and black border lines, Turns grid off
ax = sns.set_style(rc=custom_style)

def boxPlotBlackBorder(ax):
    # iterate over boxes in the plot to make each line black
    for i,box in enumerate(ax.artists):
        box.set_edgecolor('black')
        # box.set_facecolor('white')
    
        # iterate over whiskers and median lines
        for j in range(6*i, 6*(i+1)):
            ax.lines[j].set_color('black')

def smartPlot(data=None, x=None, y=None, hue=None, hue_order=None, 
              addBoxPair=None, ci=None, errcolor=None, capsize=None, 
              plot=None, test=None, sigLoc=None, text_format=None, 
              line_offset=None, line_offset_to_box=None, line_height=None, 
              fontsize=None, legLoc=None, verbose=None, xlabel=None, 
              ylabel=None, legendTitle=None, figName=None, folderName=None, 
              dataPoints=None):
    
    # barplot
    f, ax = plt.subplots()
    
    if plot == 'barplot':
        ax = sns.barplot(data=data, x=x, y=y, hue=hue, hue_order=hue_order, 
                         ci=ci, errcolor=errcolor, capsize=capsize)
    
    elif plot == 'boxplot':
        ax = sns.boxplot(data=data, x=x, y=y, hue=hue, hue_order=hue_order)
        
        # Adjust spacing
        adjust_box_widths(f, 0.97)
    
    # Statistical test for differences
    x_grps = list(data[x].unique()) # List of groups
    if hue != None:
        # Create combinations to compare
        box_pairs_1 = [((x_grps_i, hue_order[0]), 
                        (x_grps_i, hue_order[1])) 
                       for x_grps_i in x_grps]
        box_pairs = box_pairs_1
        
        if addBoxPair != None:
            # Additional box pairs
            box_pairs =  box_pairs_1 + addBoxPair
    
    elif hue_order != None:
        box_pairs = [(hue_order[0], hue_order[1])]
    
    #Stats results and significant differences (SR)
    SR = add_stat_annotation(ax, plot=plot, data=data, x=x, y=y, hue=hue, 
                             hue_order=hue_order, box_pairs=box_pairs, 
                             test=test, loc=sigLoc, text_format=text_format, 
                             verbose=verbose, comparisons_correction=None, 
                             line_offset=line_offset, 
                             line_offset_to_box=line_offset_to_box, 
                             line_height= line_height, 
                             fontsize=fontsize) # 'bonferroni'
    
    if plot == 'boxplot':
        boxPlotBlackBorder(ax) # Make borders black
    
    
    if dataPoints == True:
        # Add data points to the box plot
        sns.stripplot(data=data, x=x, y=y, hue=hue, hue_order=hue_order, 
                      color='.5', size=5, linewidth=1, dodge=True)
        
        # gather plot attributes for legends
        handles, labels = ax.get_legend_handles_labels()
        
        if hue != None:
            l = plt.legend(handles[0:2], labels[0:2], title=legendTitle)
    
    else:
        if hue != None:
            ax.legend(loc=legLoc).set_title(legendTitle)
    
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax = sns.despine() # takes the lines off on the right and top of the graph
    
    if folderName != None:
        # If a new folder name is given, put the files there
        
        # New file path
        NP = os.path.join(HSF, folderName)
        
        # Create folder if it doesn't exist
        os.makedirs(NP, exist_ok=True)
        
    else:
        # Put the file in the same folder
        NP = HSF
        
    f.savefig(os.path.join(NP, '{}.pdf'.format(figName)), 
              bbox_inches='tight')
    plt.close()

# Special spacing

def adjust_box_widths(g, fac):
    """
    Adjust the withs of a seaborn-generated boxplot.
    """
    
    # iterating through Axes instances
    for ax in g.axes:
        
        # iterating through axes artists:
        for c in ax.get_children():
            
            # searching for PathPatches
            if isinstance(c, PathPatch):
                # getting current width of box:
                p = c.get_path()
                verts = p.vertices
                verts_sub = verts[:-1]
                xmin = np.min(verts_sub[:, 0])
                xmax = np.max(verts_sub[:, 0])
                xmid = 0.5*(xmin + xmax)
                xhalf = 0.5*(xmax - xmin)
                
                # setting new width of box
                xmin_new = xmid - fac*xhalf
                xmax_new = xmid + fac*xhalf
                verts_sub[verts_sub[:, 0] == xmin, 0] = xmin_new
                verts_sub[verts_sub[:, 0] == xmax, 0] = xmax_new
                
                # setting new width of median line
                for l in ax.lines:
                    if np.all(l.get_xdata() == [xmin, xmax]):
                        l.set_xdata([xmin_new, xmax_new])

# In[Max peel force by age group]

pivotMaxForceAgeGroup = pd.pivot_table(df, values=mpf_mN, index=[AG, R], 
                                       aggfunc=pvtOut)

print('pivotMaxForceAgeGroup')
print(pivotMaxForceAgeGroup)
# Add the index groups and convert NaN's to "-"'s
print(pivotMaxForceAgeGroup.to_latex(index=True, 
                                     na_rep='-', escape=False, 
                                     float_format="{:0.3f}".format))

Folder = 'MaxPeelAgeDecade'

# Barplot
smartPlot(data=df, x=AG, y=mpf_mN, hue=R, hue_order=[Eq, Po], ci='sd', 
          errcolor='black', capsize=.2, plot='barplot', test='t-test_ind', 
          sigLoc='outside', text_format='star', line_offset=0.0, 
          line_offset_to_box=0.0, line_height=0.015, fontsize='small', 
          legLoc='best', verbose=2, 
          xlabel=A_G, ylabel=MmN, legendTitle=R, 
          figName='BarPlot', folderName=Folder)

# Boxplot
smartPlot(data=df, x=AG, y=mpf_mN, hue=R, hue_order=[Eq, Po], plot='boxplot', 
          test='t-test_ind', text_format='star', sigLoc='outside', 
          line_offset=0.0, line_offset_to_box=0.0, line_height=0.015, 
          fontsize='small', legLoc='best', verbose=2, 
          xlabel=A_G, ylabel=MmN, 
          legendTitle=R, figName='BoxPlot', folderName=Folder)

# Boxplot with data
smartPlot(data=df, x=AG, y=mpf_mN, hue=R, hue_order=[Eq, Po], plot='boxplot', 
          test='t-test_ind', sigLoc='outside', text_format='star', 
          line_offset=0.0, line_offset_to_box=0.0, line_height=0.015, 
          fontsize='small', legLoc='best', verbose=2, 
          xlabel=A_G, ylabel=MmN, 
          legendTitle=R, figName='BoxPlot_WithData', folderName=Folder, 
          dataPoints=True)

# In[Steady state peel force by age group decade]

pivotSSForceAgeGroup = pd.pivot_table(df, values=ss_mN, index=[AG, R], 
                                      aggfunc=pvtOut)

print('pivotSSForceAgeGroup')
print(pivotSSForceAgeGroup)
# Add the index groups and convert NaN's to "-"'s
print(pivotSSForceAgeGroup.to_latex(index=True, 
                                    na_rep='-', escape=False, 
                                    float_format="{:0.3f}".format))

Folder = 'SteadyStatePeelAgeDecade'

addBoxPair = [((LAG[0], Eq), (Age_Group_i, Eq)) 
               for Age_Group_i in list(df[AG].unique())]

# Barplot
smartPlot(data=df, x=AG, y=ss_mN, hue=R, hue_order=[Eq, Po], ci='sd', 
          errcolor='black', capsize=.2, addBoxPair=addBoxPair, 
          plot='barplot', test='t-test_ind', 
          sigLoc='outside', text_format='star', line_offset=0.0, 
          line_offset_to_box=0.0, line_height=0.015, fontsize='small', 
          legLoc='best', verbose=2, 
          xlabel=A_G, ylabel=ss_mN, legendTitle=R, 
          figName='BarPlot', folderName=Folder)

# Boxplot
smartPlot(data=df, x=AG, y=ss_mN, hue=R, hue_order=[Eq, Po], 
          addBoxPair=addBoxPair, plot='boxplot', 
          test='t-test_ind', text_format='star', sigLoc='outside', 
          line_offset=0.0, line_offset_to_box=0.0, line_height=0.015, 
          fontsize='small', legLoc='best', verbose=2, 
          xlabel=A_G, ylabel=ss_mN, 
          legendTitle=R, figName='BoxPlot', folderName=Folder)

# Boxplot with data
smartPlot(data=df, x=AG, y=ss_mN, hue=R, hue_order=[Eq, Po], 
          addBoxPair=addBoxPair, plot='boxplot', 
          test='t-test_ind', sigLoc='outside', text_format='star', 
          line_offset=0.0, line_offset_to_box=0.0, line_height=0.015, 
          fontsize='small', legLoc='best', verbose=2, 
          xlabel=A_G, ylabel=ss_mN, legendTitle=R, 
          figName='BoxPlot_WithData', folderName=Folder, 
          dataPoints=True)

# In[Steady state peel force by age group +/- 60]

pivotSSForceAge60 = pd.pivot_table(df, values=ss_mN, index=[A60, R], 
                                   aggfunc=pvtOut)

print('pivotSSForceAge60')
print(pivotSSForceAge60)
# Add the index groups and convert NaN's to "-"'s
print(pivotSSForceAge60.to_latex(index=True, 
                                 na_rep='-', escape=False, 
                                 float_format="{:0.3f}".format))

Folder = 'SteadyStatePeelAge60'

# Barplot
smartPlot(data=df, x=A60, y=ss_mN, hue=R, hue_order=[Eq, Po], ci='sd', 
          errcolor='black', capsize=.2, plot='barplot', test='t-test_ind', 
          sigLoc='outside', text_format='star', line_offset=0.0, 
          line_offset_to_box=0.0, line_height=0.015, fontsize='small', 
          legLoc='best', verbose=2, 
          xlabel=A_G, ylabel=ss_mN, legendTitle=R, 
          figName='BarPlot', folderName=Folder)

# Boxplot
smartPlot(data=df, x=A60, y=ss_mN, hue=R, hue_order=[Eq, Po], plot='boxplot', 
          test='t-test_ind', text_format='star', sigLoc='outside', 
          line_offset=0.0, line_offset_to_box=0.0, line_height=0.015, 
          fontsize='small', legLoc='best', verbose=2, 
          xlabel=A_G, ylabel=ss_mN, 
          legendTitle=R, figName='BoxPlot', folderName=Folder)

# Boxplot with data
smartPlot(data=df, x=A60, y=ss_mN, hue=R, hue_order=[Eq, Po], plot='boxplot', 
          test='t-test_ind', sigLoc='outside', text_format='star', 
          line_offset=0.0, line_offset_to_box=0.0, line_height=0.015, 
          fontsize='small', legLoc='best', verbose=2, 
          xlabel=A_G, ylabel=ss_mN, 
          legendTitle=R, 
          figName='BoxPlot_WithData', folderName=Folder, 
          dataPoints=True)

# In[Max peel force by age scatterplot to show regional differences]

# Subset of data
df_subset_Eq = df[df[R] == Eq]
df_subset_Po = df[df[R] == Po]

# scatter plot
f, ax = plt.subplots()
ax = sns.scatterplot(x=A, y=mpf_mN, hue=A60, style=A60, 
                     hue_order=[Aleq60, Ag60], data=df_subset_Eq, 
                     palette='Set1', s=200, legend=True)


ax.get_legend_handles_labels()[0][0]._sizes = [200.]
ax.get_legend_handles_labels()[0][1]._sizes = [200.]

legend = ax.legend(loc='best').set_title("Equator Age group (yr.)")

# #change the marker size manually for both lines
# ax.legendHandles[0]._legmarker.set_markersize(6)
# ax.legendHandles[1]._legmarker.set_markersize(6)

# Averages for the +/- 60 age group
y_less_60_Eq = df_subset_Eq[mpf_mN][(df_subset_Eq[A] <= 60)].dropna()
y_greater_60_Eq = df_subset_Eq[mpf_mN][(df_subset_Eq[A] > 60)].dropna()
y_less_60_Po = df_subset_Po[mpf_mN][(df_subset_Po[A] <= 60)].dropna()
y_greater_60_Po = df_subset_Po[mpf_mN][(df_subset_Po[A] > 60)].dropna()

x_plot_less_60 = np.linspace(30, 60, 100)
x_plot_greater_60 = np.linspace(60, 80, 100)

# Plot averages
plt.plot(x_plot_less_60, np.mean(y_less_60_Eq)*np.ones(len(x_plot_less_60)), 
         '-.', color='r', linewidth=3) # , label=r'Age $\leq$ 60 AVG')

ax.text(np.mean(x_plot_less_60)*1.1, np.mean(y_less_60_Eq) + 0.25, 
        r'Average', color='r', horizontalalignment='left', 
        fontsize=18, weight='semibold')

plt.plot(x_plot_greater_60, 
         np.mean(y_greater_60_Eq)*np.ones(len(x_plot_greater_60)), '-.', 
         color='b', linewidth=3) #, label=r'Age $>$ 60 AVG')

ax.text(np.mean(x_plot_greater_60)*0.9, np.mean(y_greater_60_Eq) + 0.25, 
        r'Average', color='b', horizontalalignment='left', 
        fontsize=18, weight='semibold')

ax.set_xlabel(A_G)
ax.set_ylabel(MmN)
# takes the lines off on the right and top of the graph
ax = sns.despine()

# New path
NP = os.path.join(HSF, 'MaxPeelAge60')

# Create folder if it doesn't exist
os.makedirs(NP, exist_ok=True)

f.savefig(os.path.join(NP, 'ScatterPlot.pdf'), 
          bbox_inches='tight')
plt.close()

tstat, pval = stats.ttest_ind(y_less_60_Eq, y_greater_60_Eq)
print('Statistical significance between Equatorial age group +/- 60 yrs.  ' + 
      f'p = {pval:.4f}')

tstat, pval = stats.ttest_ind(y_less_60_Po, y_greater_60_Po)
print('Statistical significance between Posterior age group +/- 60 yrs.  ' + 
      f'p = {pval:.4f}')

# In[Max peel force by age +/- 60]

pivotMaxForceAge60 = pd.pivot_table(df, values=mpf_mN, index=[A60, R], 
                                    aggfunc=pvtOut)

print('pivotMaxForceAge60')
print(pivotMaxForceAge60)
# Add the index groups and convert NaN's to "-"'s
print(pivotMaxForceAge60.to_latex(index=True, 
                                  na_rep='-', escape=False, 
                                  float_format="{:0.3f}".format))

Folder = 'MaxPeelAge60'

# Barplot
smartPlot(data=df, x=A60, y=mpf_mN, hue=R, hue_order=[Eq, Po], ci='sd', 
          errcolor='black', capsize=.2, plot='barplot', test='t-test_ind', 
          sigLoc='outside', text_format='star', line_offset=0.0, 
          line_offset_to_box=0.0, line_height=0.015, fontsize='small', 
          legLoc='best', verbose=2, 
          xlabel=A_G, ylabel=MmN, legendTitle=R, 
          figName='BarPlot', folderName=Folder)

# Boxplot
smartPlot(data=df, x=A60, y=mpf_mN, hue=R, hue_order=[Eq, Po], plot='boxplot', 
          test='t-test_ind', text_format='star', sigLoc='outside', 
          line_offset=0.0, line_offset_to_box=0.0, line_height=0.015, 
          fontsize='small', legLoc='best', verbose=2, 
          xlabel=A_G, ylabel=MmN, 
          legendTitle=R, figName='BoxPlot', folderName=Folder)

# Boxplot with data
smartPlot(data=df, x=A60, y=mpf_mN, hue=R, hue_order=[Eq, Po], plot='boxplot', 
          test='t-test_ind', sigLoc='outside', text_format='star', 
          line_offset=0.0, line_offset_to_box=0.0, line_height=0.015, 
          fontsize='small', legLoc='best', verbose=2, 
          xlabel=A_G, ylabel=MmN, 
          legendTitle=R, figName='BoxPlot_WithData', folderName=Folder, 
          dataPoints=True)

# In[One way anova]

# stats f_oneway functions takes the groups as input and returns F and P-value
f, p = stats.f_oneway(df[ss_mN][(df[AG] == LAG[0]) & (df[R] == Eq)].dropna(), 
                      df[ss_mN][(df[AG] == LAG[1]) & (df[R] == Eq)].dropna(), 
                      df[ss_mN][(df[AG] == LAG[2]) & (df[R] == Eq)].dropna(), 
                      df[ss_mN][(df[AG] == LAG[3]) & (df[R] == Eq)].dropna(), 
                      df[ss_mN][(df[AG] == LAG[4]) & (df[R] == Eq)].dropna())

print(f, p)
print('Statistical significance using One-way ANOVA between equatorial ' + 
      f'steady-state peel force.  p = {p:.4f}')

# boxplot
f, ax = plt.subplots()
ax = sns.boxplot(x=AG, y=ss_mN, data=df[df[R] == Eq], palette='Set1')

# Statistical test for differences
# List of groups (AgeGroups)
hue_order = list(df[AG].unique())
# Create combinations to compare
box_pairs_1 = [((LAG[0], Eq), (Age_Group_i, Eq)) 
               for Age_Group_i in hue_order]
box_pairs = box_pairs_1
test_results = add_stat_annotation(ax, plot='boxplot', data=df[df[R] == Eq], 
                                   x=AG, y=ss_mN, 
                                   hue=R, box_pairs=box_pairs,
                                   test='t-test_ind', text_format='star',
                                   loc='outside', verbose=2, 
                                   comparisons_correction=None, 
                                   line_offset=0.0, 
                                   line_offset_to_box=0.0, 
                                   line_height= 0.015, 
                                   fontsize='small') # 'bonferroni'

boxPlotBlackBorder(ax) # Make borders black

# Add data points to the box plot
sns.stripplot(x=AG, y=ss_mN, data=df[df[R] == Eq], color='.5', size=5, 
              linewidth=1, dodge=True)

# $p={:.6f}$".format(p)
ax.legend(loc='best').set_title(r"One-way ANOVA between Equatorial " + 
                                "age groups $^{\clubsuit}$")

ax.set_xlabel(A_G)
ax.set_ylabel(ss_mN)
# takes the lines off on the right and top of the graph
ax = sns.despine() 

# New path
NP = os.path.join(HSF, 'SteadyStatePeelAgeDecade')

# Create folder if it doesn't exist
os.makedirs(NP, exist_ok=True)

f.savefig(os.path.join(NP, 'Equator_BoxPlot' + 
                       'Plot.pdf'), 
          bbox_inches='tight')

# In[Chi squared test Failure Code by Region]

# Remove all NaN's from the data for regressions
# remove nans from 'failure code'
df_nN = df.dropna(subset=[fc])
pd.options.mode.chained_assignment = None  # default='warn'

# Convert to integers
df_nN[FC] = df_nN[fc].astype(int)

# Chi square data is the frequency in each group (not specific to regions)
chiSquareData = [sum(df_nN[FC] == 0), 
                 sum(df_nN[FC] == 1), 
                 sum(df_nN[FC] == 2), 
                 sum(df_nN[FC] == 3)]

# Chi square data is the frequency in each group (specific to regions Equator 
# and Posterior)
# chiSquareDataRegion
cSDR = [df_nN[FC][(df_nN[FC] == 0) & (df_nN[R] == Eq)].count(), 
        df_nN[FC][(df_nN[FC] == 1) & (df_nN[R] == Eq)].count(), 
        df_nN[FC][(df_nN[FC] == 2) & (df_nN[R] == Eq)].count(), 
        df_nN[FC][(df_nN[FC] == 3) & (df_nN[R] == Eq)].count(), 
        df_nN[FC][(df_nN[FC] == 0) & (df_nN[R] == Po)].count(), 
        df_nN[FC][(df_nN[FC] == 1) & (df_nN[R] == Po)].count(), 
        df_nN[FC][(df_nN[FC] == 2) & (df_nN[R] == Po)].count(), 
        df_nN[FC][(df_nN[FC] == 3) & (df_nN[R] == Po)].count()]

# Compare to the null hypothesis (assumption that equal distribution (mean))
chisq, pvalue = stats.chisquare(chiSquareData)

# Compare to the null hypothesis by region (assumption that equal 
# distribution (mean))
chisqRegion, pvalueRegion = stats.chisquare(cSDR)

# countplot
f, ax = plt.subplots()
# Standard Error of the mean
# (If failure codes 0, 1, 2 use FC instead of FCw)
ax = sns.countplot(x=FCw, hue=R, hue_order=[Eq, Po], data=df_nN, 
                   order=ILM_Failure_Type)

# Wrap text labels in the x-axis
max_width = 20 # Character width
ax.set_xticklabels(textwrap.fill(x.get_text(), 
                                 max_width) for x in ax.get_xticklabels())

# '{}'.format(FailureCodei)
# Statistical test for differences
# List of groups (AgeGroups)
hue_order = list(df_nN[FC].unique()) 
# Create combinations to compare
box_pairs_1 = [((FailureCodei, Eq), (FailureCodei, Po)) 
               for FailureCodei in hue_order]
box_pairs = box_pairs_1
test_results = add_stat_annotation(ax, plot = 'countplot', data=df_nN, 
                                   x=FC, y=fc, 
                                   hue=R, hue_order=[Eq, Po], 
                                   box_pairs=box_pairs, 
                                   test='chisquare', text_format='star', 
                                   loc='outside', verbose=2, 
                                   comparisons_correction=None, 
                                   line_offset=0.0, 
                                   line_offset_to_box=0.0, 
                                   line_height= 0.015, 
                                   fontsize='small') # 'bonferroni'


ax.set_ylabel(r'Count')
plt.axhline(np.mean(cSDR), ls='--', color='black', label='Null')
plt.text(2, 5, r'Average Count', color='k', horizontalalignment='left', 
         fontsize=12, weight='semibold')

# New path
NP = os.path.join(HSF, 'LightMicroscopy')

# Create folder if it doesn't exist
os.makedirs(NP, exist_ok=True)

f.savefig(os.path.join(NP, 'RegionCountPlot.pdf'), 
          bbox_inches='tight')

pivotFailureCode = pd.pivot_table(df_nN, values=fc, index=[FC, R], 
                                  aggfunc={'count'})

print('pivotFailureCode')
print(pivotFailureCode)
# Add the index groups and convert NaN's to "-"'s
print(pivotFailureCode.to_latex(index=True, na_rep='-', escape=False))


# Contingency table
# , normalize = 'index'
contigency = pd.crosstab(df_nN[R], df_nN[FC])

f, ax = plt.subplots()
ax = sns.heatmap(contigency, annot=True, cmap="YlGnBu")
f.savefig(os.path.join(NP, 'RegionContingency.pdf'), 
          bbox_inches='tight')

# Chi-square test of independence.
c, p, dof, expected = stats.chi2_contingency(contigency)

# In[Chi squared test failure code +/- 60]

# Remove all NaN's from the data for regressions
# remove nans from 'failure code'
df_nN = df.dropna(subset=[fc])
pd.options.mode.chained_assignment = None  # default='warn'

# Convert to integers
df_nN[FC] = df_nN[fc].astype(int)

# Chi square data is the frequency in each group (specific to regions Equator 
# and Posterior)
# chiSquareDataRegion
cSDR = [df_nN[FC][(df_nN[FC] == 0) & (df_nN[A60] == Aleq60)].count(), 
        df_nN[FC][(df_nN[FC] == 1) & (df_nN[A60] == Aleq60)].count(), 
        df_nN[FC][(df_nN[FC] == 2) & (df_nN[A60] == Aleq60)].count(), 
        df_nN[FC][(df_nN[FC] == 3) & (df_nN[A60] == Aleq60)].count(), 
        df_nN[FC][(df_nN[FC] == 0) & (df_nN[A60] == Ag60)].count(), 
        df_nN[FC][(df_nN[FC] == 1) & (df_nN[A60] == Ag60)].count(), 
        df_nN[FC][(df_nN[FC] == 2) & (df_nN[A60] == Ag60)].count(), 
        df_nN[FC][(df_nN[FC] == 3) & (df_nN[A60] == Ag60)].count()]

# Compare to the null hypothesis by region (assumption that equal 
# distribution (mean))
chisqRegion, pvalueRegion = stats.chisquare(cSDR)

# countplot
f, ax = plt.subplots()
# Standard Error of the mean ('sd')
# (If failure codes 0, 1, 2 use FC instead of FCw)
ax = sns.countplot(x=FCw, hue=A60, hue_order=[Aleq60, Ag60], data=df_nN, 
                   order=ILM_Failure_Type)

# Wrap text labels in the x-axis
max_width = 20
ax.set_xticklabels(textwrap.fill(x.get_text(), 
                                 max_width) for x in ax.get_xticklabels())

# Statistical test for differences
# List of groups (AgeGroups)
hue_order = list(df_nN[FC].unique())
# Create combinations to compare
box_pairs_1 = [((FailureCodei, Aleq60), (FailureCodei, Ag60)) 
               for FailureCodei in hue_order]
box_pairs = box_pairs_1
test_results = add_stat_annotation(ax, plot = 'countplot', data=df_nN, 
                                   x=FC, y=fc, 
                                   hue=A60, hue_order=[Aleq60, Ag60], 
                                   box_pairs=box_pairs, 
                                   test='chisquare', text_format='star', 
                                   loc='outside', verbose=2, 
                                   comparisons_correction=None, 
                                   line_offset=0.0, 
                                   line_offset_to_box=0.0, 
                                   line_height= 0.015, 
                                   fontsize='small') # 'bonferroni'

ax.legend(loc='best').set_title(A_G)
ax.set_ylabel(r'Count')
plt.axhline(np.mean(cSDR), ls='--', color='black', label='Null')
plt.text(2, 5, r'Average Count', color='k', horizontalalignment='left', 
         fontsize=12, weight='semibold')

# New path
NP = os.path.join(HSF, 'LightMicroscopy')

# Create folder if it doesn't exist
os.makedirs(NP, exist_ok=True)

f.savefig(os.path.join(NP, 'Age60_CountPlot.pdf'), 
          bbox_inches='tight')

pivotFailureCode = pd.pivot_table(df_nN, values=fc, index=[FC, A60], 
                                  aggfunc={'count'}) # 

print('pivotFailureCode')
print(pivotFailureCode)
# Add the index groups and convert NaN's to "-"'s
print(pivotFailureCode.to_latex(index=True, na_rep='-', escape=False))


# Contingency table
# , normalize = 'index'
contigency = pd.crosstab(df_nN[A60], df_nN[FC])

f, ax = plt.subplots()
ax = sns.heatmap(contigency, annot=True, cmap="YlGnBu")
f.savefig(os.path.join(NP, 'Age60_Contingency.pdf'), 
          bbox_inches='tight')

# Chi-square test of independence.
c, p, dof, expected = stats.chi2_contingency(contigency)

# In[Chi squared test failure code +/- 60 Posterior Pole]

# Remove all NaN's from the data for regressions
# remove nans from 'failure code'
df_nN = df.dropna(subset=[fc])
pd.options.mode.chained_assignment = None  # default='warn'

# Convert to integers
df_nN[FC] = df_nN[fc].astype(int)

# only look at posterior region
df_nN = df_nN[df_nN[R] == Po]

# Chi square data is the frequency in each group (specific to regions 
# Equator and Posterior)
# chiSquareDataRegion
cSDR = [df_nN[FC][(df_nN[FC] == 0) & (df_nN[A60] == Aleq60)].count(), 
        df_nN[FC][(df_nN[FC] == 1) & (df_nN[A60] == Aleq60)].count(), 
        df_nN[FC][(df_nN[FC] == 2) & (df_nN[A60] == Aleq60)].count(), 
        df_nN[FC][(df_nN[FC] == 3) & (df_nN[A60] == Aleq60)].count(), 
        df_nN[FC][(df_nN[FC] == 0) & (df_nN[A60] == Ag60)].count(), 
        df_nN[FC][(df_nN[FC] == 1) & (df_nN[A60] == Ag60)].count(), 
        df_nN[FC][(df_nN[FC] == 2) & (df_nN[A60] == Ag60)].count(), 
        df_nN[FC][(df_nN[FC] == 3) & (df_nN[A60] == Ag60)].count()]

# Compare to the null hypothesis by region (assumption that equal 
# distribution (mean))
chisqRegion, pvalueRegion = stats.chisquare(cSDR)

# countplot
f, ax = plt.subplots()
# Standard Error of the mean
# (If failure codes 0, 1, 2 use FC instead of FCw)
ax = sns.countplot(x=FCw, hue=A60, hue_order=[Aleq60, Ag60], 
                   data=df_nN, order=ILM_Failure_Type)

# Wrap text labels in the x-axis
max_width = 20
ax.set_xticklabels(textwrap.fill(x.get_text(), 
                                 max_width) for x in ax.get_xticklabels())

# Statistical test for differences
# List of groups (AgeGroups)
hue_order = list(df_nN[FC].unique())
# Create combinations to compare
box_pairs_1 = [((FailureCodei, Aleq60), (FailureCodei, Ag60)) 
               for FailureCodei in hue_order]
box_pairs = box_pairs_1
test_results = add_stat_annotation(ax, plot = 'countplot', data=df_nN, 
                                   x=FC, y=fc, 
                                   hue=A60, hue_order=[Aleq60, Ag60], 
                                   box_pairs=box_pairs, 
                                   test='chisquare', text_format='star', 
                                   loc='outside', verbose=2, 
                                   comparisons_correction=None, 
                                   line_offset=0.0, 
                                   line_offset_to_box=0.0, 
                                   line_height= 0.015, 
                                   fontsize='small') # 'bonferroni'

ax.legend(loc='best').set_title("Posterior pole age group (yr.)")
ax.set_ylabel(r'Count')
plt.axhline(np.mean(cSDR), ls='--', color='black', label='Null')
plt.text(2, 3, r'Average Count', color='k', horizontalalignment='left', 
         fontsize=12, weight='semibold')

# New path
NP = os.path.join(HSF, 'LightMicroscopy')

# Create folder if it doesn't exist
os.makedirs(NP, exist_ok=True)

f.savefig(os.path.join(NP, 'Age60PosteriorCountPlot.pdf'), 
          bbox_inches='tight')

pivotFailureCode = pd.pivot_table(df_nN, values=fc, index=[FC, A60], 
                                  aggfunc={'count'}) # 

print('pivotFailureCode')
print(pivotFailureCode)
# Add the index groups and convert NaN's to "-"'s
print(pivotFailureCode.to_latex(index=True, na_rep='-', escape=False))


# Contingency table
# , normalize = 'index'
contigency = pd.crosstab(df_nN[A60], df_nN[FC])

f, ax = plt.subplots()
ax = sns.heatmap(contigency, annot=True, cmap="YlGnBu")
f.savefig(os.path.join(NP, 'Age60Posterior_Contingency.pdf'), 
          bbox_inches='tight')

# Chi-square test of independence.
c, p, dof, expected = stats.chi2_contingency(contigency)