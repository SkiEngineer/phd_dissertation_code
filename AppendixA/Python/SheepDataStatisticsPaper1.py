# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 16:42:47 2020

@author: Kiffer Creveling
"""

import pandas as pd
import os
import numpy as np
import seaborn as sns
from statannot import add_stat_annotation
import matplotlib.pyplot as plt
from matplotlib.patches import PathPatch
plt.rcParams['figure.figsize'] = [16, 10]
from scipy import stats
import pdb

# In[Read values from Database]
""" Read from the database """

df = pd.read_csv('Sheep Data.csv') # Data from JMP

""" Simplification of code """
SSF = 'SheepStatisticsFigures' # Figure directory
mpf_mN = 'Max peel force (mN)' # pandas data frame
ss_mN = 'Steady-state peel force (mN)' # pandas data frame
MmN = 'Maximum peel force (mN)' # figure labels
R = 'Region'
Eq = 'Equator'
Po = 'Posterior'
AC = 'Age Classification'
AG = 'Age Group'
fc = 'failure code' # Original data
FC = 'Failure Code' # converted data for plotting

df[mpf_mN] = df['Max Peel[N]']*1000
df[ss_mN] = df['Steady-state [N]']*1000

# Exclude the cells that have duplicates or have been exculded due to 
# video analysis
df = df[df['Excluded'] != 'yes']

# Drop NA's in age classification
df = df.dropna(subset=[AC])

# In[Order the data by the corect age classification]
old = ['preterm', 'term', 'young child', 'adult']
LAG = ['Preterm', 'Neonatal', 'Young Child', 'Adult'] # labels age group new

# Replace the existing age classifications with the new ones for plotting
df[AC] = df[AC].replace(old, LAG)

# Associate the list "LAG" with cateogrical data that has a 
# specific order
df[AG] = pd.Categorical(df[AC], ordered=True, categories=LAG)

# In[Pivot Table]
# create initial pivot table

pvtOut = {'count', np.median, np.mean, np.std} # pivot table outputs

# In[Plots]

standardError = 68 # Used for confidence intervals

sns.set_theme(context='paper', style='darkgrid', palette="Paired", 
              font_scale=2)
custom_style = {'axes.facecolor': 'white',
                 'axes.edgecolor': 'black',
                 'axes.grid': False,
                 'axes.axisbelow': True,
                 'axes.labelcolor': 'black',
                 'figure.facecolor': 'white',
                 'grid.color': '.8',
                 'grid.linestyle': '-',
                 'text.color': 'black',
                 'xtick.color': 'black',
                 'ytick.color': 'black',
                 'xtick.direction': 'out',
                 'ytick.direction': 'out',
                 'lines.solid_capstyle': 'round',
                 'patch.edgecolor': 'w',
                 'patch.force_edgecolor': True,
                 'image.cmap': 'rocket',
                 'font.family': ['sans-serif'],
                 'font.sans-serif': ['Arial', 'DejaVu Sans', 
                                     'Liberation Sans', 
                                     'Bitstream Vera Sans', 'sans-serif'],
                 'xtick.bottom': True,
                 'xtick.top': False,
                 'ytick.left': True,
                 'ytick.right': False,
                 'axes.spines.left': True,
                 'axes.spines.bottom': True,
                 'axes.spines.right': False,
                 'axes.spines.top': False}
# White background with ticks and black border lines, Turns grid off
ax = sns.set_style(rc=custom_style)

def boxPlotBlackBorder(ax):
    # iterate over boxes in the plot to make each line black
    for i,box in enumerate(ax.artists):
        box.set_edgecolor('black')
        # box.set_facecolor('white')
    
        # iterate over whiskers and median lines
        for j in range(6*i,6*(i+1)):
            ax.lines[j].set_color('black')

def smartPlot(data=None, x=None, y=None, hue=None, hue_order=None, 
              addBoxPair=None, ci=None, errcolor=None, capsize=None, 
              plot=None, test=None, sigLoc=None, text_format=None, 
              line_offset=None, line_offset_to_box=None, line_height=None, 
              fontsize=None, legLoc=None, verbose=None, xlabel=None, 
              ylabel=None, legendTitle=None, figName=None, folderName=None, 
              dataPoints=None):
    
    # barplot
    f, ax = plt.subplots()
    
    if plot == 'barplot':
        ax = sns.barplot(data=data, x=x, y=y, hue=hue, hue_order=hue_order, 
                         ci=ci, errcolor=errcolor, capsize=capsize)
    
    elif plot == 'boxplot':
        ax = sns.boxplot(data=data, x=x, y=y, hue=hue, hue_order=hue_order)
        
        # Adjust spacing
        adjust_box_widths(f, 0.97)
    
    # Statistical test for differences
    x_grps = list(data[x].unique()) # List of groups
    if hue != None:
        # Create combinations to compare
        box_pairs_1 = [((x_grps_i, hue_order[0]), 
                        (x_grps_i, hue_order[1])) 
                       for x_grps_i in x_grps]
        box_pairs = box_pairs_1
        
        if addBoxPair != None:
            # Additional box pairs
            box_pairs =  box_pairs_1 + addBoxPair
    
    elif hue_order != None:
        box_pairs = [(hue_order[0], hue_order[1])]
    
    #Stats results and significant differences (SR)
    SR = add_stat_annotation(ax, plot=plot, data=data, x=x, y=y, hue=hue, 
                             hue_order=hue_order, box_pairs=box_pairs, 
                             test=test, loc=sigLoc, text_format=text_format, 
                             verbose=verbose, comparisons_correction=None, 
                             line_offset=line_offset, 
                             line_offset_to_box=line_offset_to_box, 
                             line_height= line_height, 
                             fontsize=fontsize) # 'bonferroni'
    
    if plot == 'boxplot':
        boxPlotBlackBorder(ax) # Make borders black
    
    
    if dataPoints == True:
        # Add data points to the box plot
        sns.stripplot(data=data, x=x, y=y, hue=hue, hue_order=hue_order, 
                      color='.5', size=5, linewidth=1, dodge=True)
        
        # gather plot attributes for legends
        handles, labels = ax.get_legend_handles_labels()
        
        if hue != None:
            l = plt.legend(handles[0:2], labels[0:2], title=legendTitle)
    
    else:
        if hue != None:
            ax.legend(loc=legLoc).set_title(legendTitle)
    
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax = sns.despine() # takes the lines off on the right and top of the graph
    
    if folderName != None:
        # If a new folder name is given, put the files there
        
        # New file path
        NP = os.path.join(SSF, folderName)
        
        # Create folder if it doesn't exist
        os.makedirs(NP, exist_ok=True)
        
    else:
        # Put the file in the same folder
        NP = SSF
        
    f.savefig(os.path.join(NP, '{}.pdf'.format(figName)), 
              bbox_inches='tight')
    plt.close()

# Special spacing

def adjust_box_widths(g, fac):
    """
    Adjust the withs of a seaborn-generated boxplot.
    """
    
    # iterating through Axes instances
    for ax in g.axes:
        
        # iterating through axes artists:
        for c in ax.get_children():
            
            # searching for PathPatches
            if isinstance(c, PathPatch):
                # getting current width of box:
                p = c.get_path()
                verts = p.vertices
                verts_sub = verts[:-1]
                xmin = np.min(verts_sub[:, 0])
                xmax = np.max(verts_sub[:, 0])
                xmid = 0.5*(xmin + xmax)
                xhalf = 0.5*(xmax - xmin)
                
                # setting new width of box
                xmin_new = xmid - fac*xhalf
                xmax_new = xmid + fac*xhalf
                verts_sub[verts_sub[:, 0] == xmin, 0] = xmin_new
                verts_sub[verts_sub[:, 0] == xmax, 0] = xmax_new
                
                # setting new width of median line
                for l in ax.lines:
                    if np.all(l.get_xdata() == [xmin, xmax]):
                        l.set_xdata([xmin_new, xmax_new])

# In[Max peel force by age group]

pivotMaxForceAgeClass = pd.pivot_table(df, values=mpf_mN, 
                                       index=[AG, R], aggfunc=pvtOut)
print('pivotMaxForceAgeClass')
print(pivotMaxForceAgeClass)
# Add the index groups and convert NaN's to "-"'s
print(pivotMaxForceAgeClass.to_latex(index=True, 
                                     na_rep='-', 
                                     float_format="{:0.3f}".format))

Folder = 'SheepMaxPeelAgeGroup'

# Barplot
smartPlot(data=df, x=AG, y=mpf_mN, hue=R, hue_order=[Eq, Po], ci='sd', 
          errcolor='black', capsize=.2, plot='barplot', test='t-test_ind', 
          sigLoc='outside', text_format='star', line_offset=0.0, 
          line_offset_to_box=0.0, line_height=0.015, fontsize='small', 
          legLoc='best', verbose=2, 
          xlabel=AG, ylabel=MmN, legendTitle=R, 
          figName='BarPlot', folderName=Folder)

# Boxplot
smartPlot(data=df, x=AG, y=mpf_mN, hue=R, hue_order=[Eq, Po], plot='boxplot', 
          test='t-test_ind', text_format='star', sigLoc='outside', 
          line_offset=0.0, line_offset_to_box=0.0, line_height=0.015, 
          fontsize='small', legLoc='best', verbose=2, 
          xlabel=AG, ylabel=MmN, 
          legendTitle=R, figName='BoxPlot', folderName=Folder)

# Add equatorial region to stats
hue_order_Eq = list(df[AG].unique()) # List of groups (AgeGroups)
addBoxPair = [((LAG[-1], Eq), (Age_Group_i, Eq)) 
               for Age_Group_i in hue_order_Eq] # Create combinations to compare

# Boxplot with data
smartPlot(data=df, x=AG, y=mpf_mN, hue=R, hue_order=[Eq, Po], 
          addBoxPair=addBoxPair, plot='boxplot', 
          test='t-test_ind', sigLoc='outside', text_format='star', 
          line_offset=0.0, line_offset_to_box=0.0, line_height=0.015, 
          fontsize='small', legLoc='best', verbose=2, 
          xlabel=AG, ylabel=MmN, 
          legendTitle=R, 
          figName='BoxPlot_WithData', folderName=Folder, 
          dataPoints=True)


# In[Steady state peel force by age group]

pSSFAgeClass = pd.pivot_table(df, values=ss_mN, index=[AG, R], 
                              aggfunc=pvtOut)

print('pSSFAgeClass')
print(pSSFAgeClass)
# Add the index groups and convert NaN's to "-"'s
print(pSSFAgeClass.to_latex(index=True, 
                            na_rep='-', 
                            float_format="{:0.3f}".format))

Folder = 'SheepSteadyStatePeelAgeGroup'

# Barplot
smartPlot(data=df, x=AG, y=ss_mN, hue=R, hue_order=[Eq, Po], ci='sd', 
          errcolor='black', capsize=.2, plot='barplot', test='t-test_ind', 
          sigLoc='outside', text_format='star', line_offset=0.0, 
          line_offset_to_box=0.0, line_height=0.015, fontsize='small', 
          legLoc='best', verbose=2, 
          xlabel=AG, ylabel=ss_mN, legendTitle=R, 
          figName='BarPlot', folderName=Folder)

# Boxplot
smartPlot(data=df, x=AG, y=ss_mN, hue=R, hue_order=[Eq, Po], plot='boxplot', 
          test='t-test_ind', text_format='star', sigLoc='outside', 
          line_offset=0.0, line_offset_to_box=0.0, line_height=0.015, 
          fontsize='small', legLoc='best', verbose=2, 
          xlabel=AG, ylabel=ss_mN, 
          legendTitle=R, 
          figName='BoxPlot', folderName=Folder)

# Boxplot with data
smartPlot(data=df, x=AG, y=ss_mN, hue=R, hue_order=[Eq, Po], plot='boxplot', 
          test='t-test_ind', sigLoc='outside', text_format='star', 
          line_offset=0.0, line_offset_to_box=0.0, line_height=0.015, 
          fontsize='small', legLoc='best', verbose=2, 
          xlabel=AG, ylabel=ss_mN, 
          legendTitle=R, 
          figName='BoxPlot_WithData', folderName=Folder, 
          dataPoints=True)

# In[One way anova]

# stats f_oneway functions takes the groups as input and returns F and P-value
f, p = stats.f_oneway(df[mpf_mN][(df[AG] == LAG[0]) & (df[R] == Eq)].dropna(), 
                      df[mpf_mN][(df[AG] == LAG[1]) & (df[R] == Eq)].dropna(), 
                      df[mpf_mN][(df[AG] == LAG[2]) & (df[R] == Eq)].dropna(), 
                      df[mpf_mN][(df[AG] == LAG[3]) & (df[R] == Eq)].dropna())
print(f, p)

# boxplot
f, ax = plt.subplots()
ax = sns.boxplot(x=AG, y=mpf_mN, data=df[df[R] == Eq], palette='Set1')

# Statistical test for differences
hue_order = list(df[AG].unique()) # List of groups (AgeGroups)
box_pairs_1 = [((LAG[-1], Eq), (Age_Group_i, Eq)) 
               for Age_Group_i in hue_order] # Create combinations to compare
box_pairs = box_pairs_1
test_results = add_stat_annotation(ax, plot='boxplot', data=df[df[R] == Eq], 
                                   x=AG, y=mpf_mN, 
                                   hue=R, box_pairs=box_pairs,
                                   test='t-test_ind', text_format='star',
                                   loc='outside', verbose=2, 
                                   comparisons_correction=None, 
                                   line_offset=0.0, 
                                   line_offset_to_box=0.0, 
                                   line_height= 0.015, 
                                   fontsize='small') # 'bonferroni'

boxPlotBlackBorder(ax) # Make borders black

# $p={:.6f}$".format(p)
ax.legend(loc='best').set_title(r"One-way ANOVA between Equatorial age " + 
                                "groups $^{\clubsuit}$")
ax.set_xlabel(AG)
ax.set_ylabel(MmN)
ax = sns.despine() # takes the lines off on the right and top of the graph

# New path
NP = os.path.join(SSF, 'SheepMaxPeelAgeGroup')

# Create folder if it doesn't exist
os.makedirs(NP, exist_ok=True)

f.savefig(os.path.join(NP, 'EquatorBoxPlot.pdf'), bbox_inches='tight')