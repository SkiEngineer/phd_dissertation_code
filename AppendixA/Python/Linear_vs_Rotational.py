# -*- coding: utf-8 -*-
"""
Created on Sat Feb 20 19:21:31 2021

@author: Kiffer Creveling
"""

import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = [16, 9]

# from matplotlib import rc, rcParams

foldPath = 'D:\Documents\Kiffer\Peel Test Paper 1\Spreadsheets for figures'

# In[Initial tape peel test data]

"""
The first set of tape peel tests where the test was exactly like the real
 tissue test.  The tape was adhered to the ball and the plastic tab was
 glued to the tape adjacent to the ball
"""
df1 = 'Specimen_RawData_1_Tape.csv'
df2 = 'Specimen_RawData_2_Tape.csv'
df3 = 'Specimen_RawData_3_Tape.csv'
df4 = 'Specimen_RawData_4_Tape.csv'
df5 = 'Specimen_RawData_5_Tape.csv'
df6 = 'Specimen_RawData_6_Tape.csv'
df7 = 'Specimen_RawData_7_Tape.csv'
df8 = 'Specimen_RawData_8_Tape.csv'
df9 = 'Specimen_RawData_9_Tape.csv'

d1 = pd.read_csv(os.path.join(foldPath, df1), sep=",", header=4, 
                 names=['Time', 'disp', 'Force', 'InterestPoint'])

d2 = pd.read_csv(os.path.join(foldPath, df2), sep=",", header=4, 
                 names=['Time', 'disp', 'Force', 'InterestPoint'])

d3 = pd.read_csv(os.path.join(foldPath, df3), sep=",", header=4, 
                 names=['Time', 'disp', 'Force', 'InterestPoint'])

d4 = pd.read_csv(os.path.join(foldPath, df4), sep=",", header=4, 
                 names=['Time', 'disp', 'Force', 'InterestPoint'])

d5 = pd.read_csv(os.path.join(foldPath, df5), sep=",", header=4, 
                 names=['Time', 'disp', 'Force', 'InterestPoint'])

d6 = pd.read_csv(os.path.join(foldPath, df6), sep=",", header=4, 
                 names=['Time', 'disp', 'Force', 'InterestPoint'])

d7 = pd.read_csv(os.path.join(foldPath, df7), sep=",", header=4, 
                 names=['Time', 'disp', 'Force', 'InterestPoint'])

d8 = pd.read_csv(os.path.join(foldPath, df8), sep=",", header=4, 
                 names=['Time', 'disp', 'Force', 'InterestPoint'])

d9 = pd.read_csv(os.path.join(foldPath, df9), sep=",", header=4, 
                 names=['Time', 'disp', 'Force', 'InterestPoint'])

# Plots
fig, ax = plt.subplots()
ax.plot(d1.Time, d1.Force, label='1')
ax.plot(d2.Time, d2.Force, label='2')
ax.plot(d3.Time, d3.Force, label='3')
ax.plot(d4.Time, d4.Force, label='4')
ax.plot(d5.Time, d5.Force, label='5')
ax.plot(d6.Time, d6.Force, label='6')
ax.plot(d7.Time, d7.Force, label='7')
ax.plot(d8.Time, d8.Force, label='8')
ax.plot(d9.Time, d9.Force, label='9')

ax.axhline(color='k', linewidth=0.25)
ax.set_ylim([0, 1.1])
ax.set_xlabel(r'Time (s)', fontsize=14)
ax.set_ylabel('Peel Force (N)', fontsize=14)
ax.legend(loc=4, fontsize=14)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.savefig('linear_Peel_Data_1.pdf', dpi=300, 
            bbox_inches='tight')
plt.show()

# In[Updated tape peel tests]
"""
Data includes the updated linear peel tests where the tape was
 initially in the grips to eliminate the toe region of the curve.  The
 rotating peel tests were done using the initial adult eye holder which did
 not have to correct ratio of pulley to diameter
"""
T1 = 'Linear_Tape_1.txt'
T2 = 'Linear_Tape_2.txt'
T3 = 'Linear_Tape_3.txt'

R1 = 'Rotate_Tape_1.txt'
R2 = 'Rotate_Tape_2.txt'
R3 = 'Rotate_Tape_3.txt'

d1 = pd.read_csv(os.path.join(foldPath, T1), sep="\t", header=1, 
                 names=['Time', 'disp', 'Force'])
t1 = d1.Time
f1 = d1.Force

d2 = pd.read_csv(os.path.join(foldPath, T2), sep="\t", header=1, 
                 names=['Time', 'disp', 'Force'])
t2 = d2.Time
f2 = d2.Force

d3 = pd.read_csv(os.path.join(foldPath, T3), sep="\t", header=1, 
                 names=['Time', 'disp', 'Force'])
t3 = d3.Time
f3 = d3.Force

d4 = pd.read_csv(os.path.join(foldPath, R1), sep="\t", header=1, 
                 names=['Time', 'disp', 'Force'])
t4 = d4.Time
f4 = d4.Force

d5 = pd.read_csv(os.path.join(foldPath, R2), sep="\t", header=1, 
                 names=['Time', 'disp', 'Force'])
t5 = d5.Time
f5 = d5.Force

d6 = pd.read_csv(os.path.join(foldPath, R3), sep="\t", header=1, 
                 names=['Time', 'disp', 'Force'])
t6 = d6.Time
f6 = d6.Force

# Plots
fig, ax = plt.subplots()
cutoff = 2000 # s
color = 'tab:blue'
ax.plot(t1[t1 < cutoff], f1[t1 < cutoff], label='Linear 1', 
        linewidth=1, color=color, alpha = 1)
ax.plot(t2[t2 < cutoff], f2[t2 < cutoff], label='Linear 2', 
        linewidth=1, color=color, alpha = 0.8)
ax.plot(t3[t3 < cutoff], f3[t3 < cutoff], label='Linear 3', 
        linewidth=1, color=color, alpha = 0.6)

color = 'tab:red'
ax.plot(t4[t4 < cutoff], f4[t4 < cutoff], label='Rotational 1', 
        linewidth=1, color=color, alpha = 1)
ax.plot(t5[t5 < cutoff], f5[t5 < cutoff], label='Rotational 2', 
        linewidth=1, color=color, alpha = 0.8)
ax.plot(t6[t6 < cutoff], f6[t6 < cutoff], label='Rotational 3', 
        linewidth=1, color=color, alpha = 0.6)

ax.axhline(color='k', linewidth=0.25)
ax.set_ylim([0, 0.85])
ax.set_xlim([0, cutoff])
ax.set_xlabel(r'Time (s)', fontsize=14)
ax.set_ylabel('Peel Force (N)', fontsize=14)
ax.legend(loc=4, fontsize=14)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.savefig('Linear_vs_Rotate_Data_2.pdf', dpi=300, 
            bbox_inches='tight')
plt.show()


# In[Updated eye holder dimension]
"""
Data set 3 includes the new adult eye holder with the correct ratio of
 pulley to diameter
"""

uR1 = 'Update_Rotate_Tape_1.txt'
uR2 = 'Update_Rotate_Tape_2.txt'
uR3 = 'Update_Rotate_Tape_3.txt'
uR4 = 'Update_Rotate_Tape_4.txt'
uR5 = 'Update_Rotate_Tape_5.txt'
uR6 = 'Update_Rotate_Tape_6.txt'
uR7 = 'Update_Rotate_Tape_7.txt'

u1 = pd.read_csv(os.path.join(foldPath, uR1), sep="\t", header=1, 
                 names=['Time', 'disp', 'Force'])
ut1 = u1.Time
uf1 = u1.Force

u2 = pd.read_csv(os.path.join(foldPath, uR2), sep="\t", header=1, 
                 names=['Time', 'disp', 'Force'])
ut2 = u2.Time
uf2 = u2.Force

u3 = pd.read_csv(os.path.join(foldPath, uR3), sep="\t", header=1, 
                 names=['Time', 'disp', 'Force'])
ut3 = u3.Time
uf3 = u3.Force

u4 = pd.read_csv(os.path.join(foldPath, uR4), sep="\t", header=1, 
                 names=['Time', 'disp', 'Force'])
ut4 = u4.Time
uf4 = u4.Force

u5 = pd.read_csv(os.path.join(foldPath, uR5), sep="\t", header=1, 
                 names=['Time', 'disp', 'Force'])
ut5 = u5.Time
uf5 = u5.Force

u6 = pd.read_csv(os.path.join(foldPath, uR6), sep="\t", header=1, 
                 names=['Time', 'disp', 'Force'])
ut6 = u6.Time
uf6 = u6.Force

u7 = pd.read_csv(os.path.join(foldPath, uR7), sep="\t", header=1, 
                 names=['Time', 'disp', 'Force'])
ut7 = u7.Time
uf7 = u7.Force

# # activate latex text rendering
# rc('text', usetex=True)
# rc('axes', linewidth=2)
# rc('font', weight='bold')

# rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']

# Plots
fig, ax = plt.subplots()
cutoff = 1400 # s

color = 'tab:blue'
ax.plot(t1[t1 < cutoff], f1[t1 < cutoff], '--', color = color, 
        label='Linear peel tests', linewidth=1, alpha = 1)
ax.plot(t2[t2 < cutoff], f2[t2 < cutoff], '--', color = color, 
        linewidth=1, alpha = 0.8)
ax.plot(t3[t3 < cutoff], f3[t3 < cutoff], '--', color = color, 
        linewidth=1, alpha = 0.6)

color = 'tab:red'
# ax.plot(ut1[ut1 < cutoff], uf1[ut1 < cutoff], label='Rotational 1', 
#         linewidth=1)
# ax.plot(ut2[ut2 < cutoff], uf2[ut2 < cutoff], label='Rotational 2', 
#         linewidth=1)
# ax.plot(ut3[ut3 < cutoff], uf3[ut3 < cutoff], label='Rotational 3', 
#         linewidth=1)
ax.plot(ut4[ut4 < cutoff], uf4[ut4 < cutoff], color = color, 
        label='Rotational peel test', linewidth=3) # Used for publication
# ax.plot(ut5[ut5 < cutoff], uf5[ut5 < cutoff], label='Rotational 5', 
#         linewidth=1)
# ax.plot(ut6[ut6 < cutoff], uf6[ut6 < cutoff], label='Rotational 6', 
#         linewidth=1)
# ax.plot(ut7[ut7 < cutoff], uf7[ut7 < cutoff], label='Rotational 7', 
#         linewidth=1)

ax.axhline(color='k', linewidth=0.25)
ax.set_ylim([0, 0.8])
ax.set_xlim([0, cutoff])
ax.set_xlabel(r'Time (s)', fontsize=14)
ax.set_ylabel('Peel Force (N)', fontsize=14)
ax.legend(loc=4, fontsize=14)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.savefig('Linear_vs_Rotate_Data_3.pdf', dpi=300, 
            bbox_inches='tight')
plt.show()
